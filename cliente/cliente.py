import zmq
import json
import os
import hashlib


context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('tcp://localhost:8888')


CHUNK_SIZE = 250000
sha1 = hashlib.sha1()

def convertToJson(user, tipo, file_dir):
    crearJson = json.dumps(
        {
            "usuario" : user,
            "tipo" : tipo,
            "filename": file_dir,
            "chunk":0
        }
    )
    return crearJson

def procesaJson(mensjson):
    jsondecoded = mensjson.decode('utf-8')
    finaljson = json.loads(jsondecoded)
    return finaljson

def downloadFile(user,file_dir,response, archivo):
    newfilepath = './Clientfiles/'+user+'/'+file_dir
    os.makedirs(os.path.dirname(newfilepath), exist_ok=True)
    numberofparts=response["numberofparts"]
    counterofparts = 1
    file = open(newfilepath, "ab")
    file.write(archivo)

    while counterofparts < numberofparts:
        porcentaje = (counterofparts*100)/(numberofparts-1)
        print(str("{:.1f}".format(porcentaje)) + '%')
        partjson = json.dumps({'part': counterofparts })
        partencoded = partjson.encode('utf-8')
        socket.send_multipart([jsonencoded, partencoded])
        mens,archivo_respuesta = socket.recv_multipart()
        archivo = archivo_respuesta
        file = open(newfilepath, "ab")
        file.write(archivo)
        counterofparts += 1
    if numberofparts == 1:
        print(str("{:.1f}".format(100)) + '%')
    file.close()

def menuDatos():

    os.system('cls||clear')
    print('1.Upload\n2.Sharelink\n3.List\n4.Download\n5.Salir\n')
    print('Seleccione una opcion: ')
    selector = str(input())
    user=''
    tipo=''
    file_dir=''
    os.system('cls||clear')
    if selector == '1':
        print('Ingrese usuario: ')
        user = str(input())
        print('\nIngrese archivo a subir: ')
        file_dir = str(input())
        tipo='upload'
    elif selector == '2':
        print('Ingrese usuario que subió el archivo: ')
        user = str(input())
        print('\nIngrese nombre del archivo que desea compartir: ')
        file_dir = str(input())
        tipo = 'sharelink'
    elif selector == '3':
        print('\n1.Solo archivos de un usuario\n2.Todos los archivos\n')
        print('Seleccione una opcion: ')
        select = str(input())
        tipo = 'list'
        if select == '1':
            file_dir = 'aaa'
            print('Ingrese usuario a buscar: ')
            user = str(input())
        elif select =='2':
            file_dir = 'todo'
            user='aaa'
        else:
            print('seleccione una opcion valida')
    elif selector == '4':
        user = ''
        print('\nIngrese el link de descarga: ')
        file_dir = str(input())
        tipo = 'downloadlink'
    elif selector == '5':
        user = ''
        file_dir=''
        tipo=''
    print('\n')
    return selector, user, tipo, file_dir

def sizeArchivo(file):
    file.seek(0, os.SEEK_END)
    print("Size of file is :", file.tell(), "bytes")
    size = file.tell()
    file.seek(0, os.SEEK_SET)
    return size

def convertToJsonHash(chunkHash,chunkCounter,file_hash):
    crearJson = json.dumps(
        {
            "hash" : chunkHash,
            "chunkCounter" : chunkCounter,
            "file_hash":file_hash
        }
    )
    return crearJson

def getFileHash(file):
    sha1Hash = hashlib.sha1(file)
    sha1Hashed = sha1Hash.hexdigest()
    return sha1Hashed


while True:

    selector, user,tipo,file_dir = menuDatos()
    newjson = convertToJson(user, tipo, file_dir)
    jsonencoded = newjson.encode('utf-8')

    if selector == '1':
        file = open(file_dir, "rb")
        readfile=file.read()
        file_size = sizeArchivo(file)
        file_hash = getFileHash(readfile)
        porcentaje = (CHUNK_SIZE*100)/file_size
        contador = 0
        chunk = file.read(CHUNK_SIZE)
        chunkCounter = 0
        while chunk:
            sha1.update(chunk)
            chunkHash= sha1.hexdigest()
            if file_size>CHUNK_SIZE:
                jsonHash = convertToJsonHash(chunkHash,chunkCounter,file_hash).encode('utf-8')
            elif file_size<=CHUNK_SIZE:
                jsonHash = convertToJsonHash(file_hash,chunkCounter,file_hash).encode('utf-8')


            socket.send_multipart([jsonencoded, chunk, jsonHash])
            chunk = file.read(CHUNK_SIZE)
            chunkCounter +=1
            mensaje = socket.recv_string()
            if mensaje=='archivoexiste':
                print(f'{user} ya subio un archivo con el nombre {file_dir}')
                chunk=False
            elif mensaje=='actualizapuntero':
                print('el archivo existia en [SERV], puntero actualizado -> subido correctamente')
                chunk=False
            else:
                contador += porcentaje
                if contador>100:
                    contador=100
                print(str("{:.1f}".format(contador)) + '%')
        file.close()

    elif selector == '2':
        socket.send_multipart([jsonencoded])
        response = socket.recv_string()
        print(response)

    elif selector == '3':
        socket.send_multipart([jsonencoded])
        response = socket.recv_string()
        print(response)

    elif selector == '4':
        partjson = json.dumps({'part': 0})
        partencoded = partjson.encode('utf-8')
        socket.send_multipart([jsonencoded,partencoded])
        mens = socket.recv_multipart()
        response = procesaJson(mens[0])
        if response["encontrado"]:
            file_dir = response["filename"]
            downloadFile(user,file_dir,response, mens[1])
        else:
            print(response["response"])
    elif selector == '5':
        break
    else:
        print('digite correctamente el comando')
    str(input('\n\npresione enter para continuar'))
