Resumen
A diferencia de otras producciones la Historia es un estudio, de ahí su estatus científico. Pese tal obviedad,
no faltan los posicionamientos periclitados que pugnan por incluir la ciencia histórica entre las artes. No se
trata tanto de evaluar cualitativamente entre ciencia y arte como de establecer cada especie en su lugar, por
mor de una mejor comprensión de lo tratado; entre otras razones porque arte y ciencia, o dentro de la ciencia
“las dos culturas”, no ganan nada con un enfrentamiento sino todo lo contrario. La transdisciplinariedad es,
pues, una necesidad mas no una excusa para realizar inversiones innecesarias, ya que el contacto enriquece
mientras que la confusión empobrece. A partir de aquí deslindaremos cuanto podamos la Historia, ciencia
que se ocupa de nuestra historia.
Palabras clave: Historia, tiempo, subjetividad, objetividad, historiar, ciencia.
Abstract
Unlike other productions the History is a study, so that scientist status. In spite of such obviousity, it’s
not lacking positions pericliteds that they struggle for include the historic sciencie between arts. We deal
with so much of qualitative evaluate between art and sciencie as establish each species in theirs spot, for
understanding’s sake best of the research; among rations other why sciencie and art, or inside the sciencie
“the two cultures”, they reach not anything with a confrontation but quite the opposite. The transdiscipliness
it’s then a need but it’s not excuse for achieve in reverse order unnecessaries, since the contact we enrich
whereas the confusion we impoverish. To from now we can divide much the History, sciencie that herself
concern your history.
Keywords: History, time, subjectivess, objectivess, tell, sciencie.
1.  Aproximaciones primarias
Nuestro título homenajea descaradamente a Reinhart Koselleck1
, entre otros motivos porque es el historiador que más se ha ocupado de la Historia (Historie) y de cómo ésta se
ocupa de la historia (Geschichte). El primer término, dado que el alemán posee dos para
1 Uno de sus libros traducidos al castellano se intitula historia/Historia, Madrid, Trotta, 2004.
210 Pascual Raga Rosaleny Historia e historia
Norba. Revista de Historia, Vol. 20, 2007, 209-224
diferenciarlos entrambos –aunque viene dándose la absorción del primero por el segundo–,
se refiere “a la historia como relato, conocimiento e investigación”2
; el segundo “corresponde
a la historia acontecida (geschehen: acontecer, suceder)”3
. Además, cuando hablamos de
historia e Historia, sin más adjetivos, entendemos por supuesto que son las humanas. Estas
primeras distinciones son muy convenientes, para entender el porqué del sinsentido de cuanto
obstat se le coloca al hecho apodíctico de la cientificidad del historiar, reparos que abordamos también por no caer en lo advertido por Burke4
, en cuanto a las vías argumentales concernientes al presente estudio. Así, se pretende reducir la Historia a un mero género literario
y a la historia a un fantasma inaprensible de cualidad acuosa, rasgo este último capital por
cuanto implica de moldeabilidad recipiental; o, lo que es lo mismo, de cuánto dependería la
historia –según esta corriente– del primar el “envase” que la contiene, como si ese continente
pudiese explicar su contenido, sin siquiera jerarquizar –en el caso de los pseudohistoriadores más extremistas– la relevancia entre la pléyade de contenedores habidos. Por cerrar esto
último, los defensores de la inexistencia de la historia, negacionistas donde los haya, se apoyan en primer lugar en las declaraciones de Einstein, quien dijo que la separación del tiempo
entre pasado, presente y futuro es una ilusión obstinada5
. De ahí se derivarían posiciones como
la de Merleau-Ponty, quien reduce el tiempo a nuestra propia conciencia temporalizadora6
,
o la más radical hasta el momento de la mano de Barbour, quien postula la inexistencia del
tiempo7
. Para el caso de la Historia literaturizada su mayor exponente viene siendo White8
,
preceptuador de la línea que entiende la Historia como una realización retórica propia de su
sincronía, borrando así cualquier distinción entre ficción e historiación al entenderse ambas
como labores literarias.
El quid de la cuestión es que late un problema de fondo, problemática inserta ya en la
misma anagoge9
 que vivimos, y cuyas trazas vienen especialmente marcadas por el asalto al
Absoluto. Este ataque viene dándose en nuestra época posmoderna con el auge del relativismo,
signo del cambio de Era donde destaca “la tarea de construir un nuevo orden mejor para
reemplazar al viejo y defectuoso”10; principiado filosóficamente dicho cambio con Nietzsche,
como primer presentador de la divergencia del horizonte humano11, podemos definir la posmodernidad como la época que “no acepta la existencia de realidad alguna que se presente
como absoluta, autónoma y suficiente, trátese de Dios, del Hombre o de la Razón”12. La pugna
entre Absoluto y Relativo no es de hoy, presente como lo estaba ya en tiempos de Sócrates,
mas la novedad es el triunfo del mismo en nuestro tiempo. Así, se entiende bien la atribución
fluida a la historia fantasmatizada posmodernamente, ya que en lugar de allegarnos lo pasado
2 Gómez, Antonio, en la introducción al trabajo citado en la nota anterior, p. 23. 3 Íbídem. 4 Burke, Kenneth: Permanence and Change, Nueva York, New Republic, 1935, p. 70, donde dice que “una
forma de ver constituye también una forma de no ver: la concentración en el objeto A implica el descuido del objeto B”. 5 En Speziali, Pierre (ed.): Albert Einstein-Michele Besso, correspondance, París, Hermann, 1972, p. 537. 6 Merleau-Ponty, Maurice: Fenomenología de la percepción, Barcelona, Planeta-Agostini, 1984,
pp. 424-425.
7 Barbour, Julián B.: The End of Time. The next Revolution in Physics, Oxford, O.U.P., 2000. 8 White, Hayden: El contenido de la forma: narrativa, discurso y representación histórica, Barcelona, Paidós,
1992 y El texto histórico como artefacto literario, Barcelona, Paidós, 2003. 9 Entendida ésta al modo de Jameson, Fredric: Documentos de cultura, documentos de barbarie. La narrativa como acto socialmente simbólico, Madrid, Visor, 1989, p. 26, quien la alude tal la conformación del referencial
trasfondo epocal dado, resultado de la experiencia histórica colectiva. 10 Baumann, Zygmunt; Modernidad líquida, Buenos Aires, F.C.E., 2006, p. 11. 11 Fullat, Octavi: El siglo posmoderno (1900-2001), Barcelona, Crítica, p. 18. 12 Ídem, p. 27.
Norba. Revista de Historia, Vol. 20, 2007, 209-224
Historia e historia Pascual Raga Rosaleny 211
–objetivo de la Historia– desde una actualización necesariamente anacrónica, como sí hacían
los historiadores de otras épocas (de tal manera que se ayudaba a penetrar la sustancia del
acaecimiento remoto), la actual anacronización se aplica a la sustancialidad de la historia,
generando de este modo narrativas en vez de historiaciones. Además, la mentada anagoge
recibe el refuerzo del quórum de la comunidad científica13, que determina para nuestro hogaño el aplauso al Relativismo, entre otras razones por la influencia del individualismo –en
lo social– y de las teorías de la relatividad y la más reciente del caos14, así como del principio
de indeterminación de Heisenberg –en lo intelectual–.
La infeliz frase: “la historia la escriben los vencedores”, de autor anónimo, es una tautología porque no viene más que a repetir con cierta aspiración de verosimilitud los reproches de similar catadura extendidos en todo tiempo; para empezar, lo que se escribe es la
Historia, escritura ancilar a más, ya que la historia es la vida misma del hombre, que podremos conocer mejor o peor pero es la que ha sido independientemente de nuestro conocimiento. Hoy, menos que nunca, se sostiene la anterior cita u otras por el estilo15, como la
atribuida a Napoleón, cuando califica a la historia de “sencilla fábula que todos hemos
aceptado”. Lo que sí es colegible es una voluntad de imposición de muchas versiones oficiales, interesadas manipulaciones ideológicas que revelan los mecanismos del afán de poder16. A este entuerto malicioso contribuyen, más o menos de forma consciente, desde las
llamadas novelas históricas a cuanto “ensayismo […] ha asaltado las bardas del viejo huerto
de Clío […] desdeñando la áspera senda de la investigación [en favor de] elucubraciones
fantasiosas […] y hasta paparruchas”17. Empero, que haya pruebas erróneas no quiere decir
que no haya historiaciones ejemplares, puesto que no sólo la falsación, sino el ir tomando
las elaboraciones o algunas de sus partes, que van apareciendo como más congruentes con la
realidad18, es la tarea de toda ciencia; desde las idolatradas matemáticas y física, a las denostadas Sociología e Historia, toda ciencia establece unos paradigmas válidos, mas nunca definitivos por siempre incompletos, abiertos pues, en espera de otros mejores que sustituyan
a los establecidos.
Cerrando este epígrafe hemos de concluir que nuestra tarea es historiar. Historiar no es
hacer historia –porque la historia ya está hecha–, sino allegarnos los aconteceres del modo
más riguroso y enriquecido que podamos. No se trata, entonces, tanto del proponer una posible Historia (posible entre otras) como del elucidar el historiamiento más cercano a los hechos sucedidos; hechos entendidos no sólo como los despuntes más vistosos de sí mismos,
sino como la completud omniabarcante que le es propia como conjunto, habida cuenta la
interrelación de todos los hechos que constituyen la realidad historiable que llamamos historia. Concurre aquí la misma relación que tiene el hombre con la naturaleza, es decir, que
al tiempo que se adapta al medio lo transforma, coextensiva así a la historia, pues al tiempo
que se adapta al acontecer (con sus ritmos estructurados y coyunturalmente anagógicos) lo
transforma a golpes acontecimientales.
13 Vid. Kuhn, Thomas S.: La estructura de las revoluciones científicas, Madrid, F.C.E., 1987. 14 Apadrinada por Lorenz, Mandelbrot, Feigenbaum y otros. Curiosamente, Caos no está entendido en este
campo de investigación como ausencia de orden, sino como cierto tipo de orden de características impredecibles,
aunque con posibilidad de ser descritas de forma concreta y precisa. 15 Pues, como dice Koselleck, Reinhart, en: Los estratos del tiempo: estudios sobre la historia, Barcelona,
Paidós, 2001, p. 82: “Es un principio de experiencia acreditable que a corto plazo la historia la hacen los vencedores,
a medio plazo probablemente se mantenga así y a largo plazo no hay quien la controle”. 16 Cfr. Foucault, Michel: Vigilar y castigar: nacimiento de la prisión, Madrid, Siglo XXI, 2000. 17 Sánchez-Albornoz, Claudio: Historia y libertad. Ensayos sobre historiología, Valencia, Júcar, 1978, p. 11. 18 Como muy bien expone Elias, Norbert: Teoría del símbolo, Barcelona, Península, 2000. 
212 Pascual Raga Rosaleny Historia e historia
Norba. Revista de Historia, Vol. 20, 2007, 209-224
2. ¿Qué es la Historia?
Señalábamos al principio la obviedad de esta cuestión, pero ocurre lo que con tino distingue Zubiri, ya que “obvio no es aquello que se entiende sin más, sino lo que uno se encuentra al paso cuando va hacia algo”19. Desde que Herodoto (480-430 a.C.) emplease por vez
primera el concepto como “investigación mediante la formulación de las correspondientes
preguntas sobre el pasado humano”20, tenemos definiciones tan entusiastas como la de Cicerón (55 a.C.): “La Historia es testimonio del tiempo, luz de la verdad, vida de la memoria,
maestra de la vida, reflejo de la antigüedad”21, y tan taciturnas como la de Gibbon (1776):
“La Historia es, en efecto, poco más que el registro de los crímenes, locuras y adversidades
de la humanidad”22. Mas éstas, como otras, adolecen de generalización o de parcialidad, con
lo que tendremos que esperar a la ya clásica de Bloch (1944): “La Historia es la ciencia de
los hombres en el tiempo”23, como subsumidora y acicateadora de otras, tal la de Ortega y
Gasset (1941): “La misión de la Historia es hacernos verosímiles los otros hombres”24, la de
Sánchez-Albornoz (1943): “La Historia es la ciencia de los ‘porqués’ ”25, la de Koselleck (1975),
quien contempla la Historia como una: “Histórica […], una categoría trascendental que reúne
las condiciones de una historia posible con las de su conocimiento”26, la de Mendieta, cuando
dice que la: “Historia no se efectúa en el tiempo sino a través del tiempo”27, o la de Klauer,
quien la entiende como: “el estudio científico de cómo los pueblos, a través del tiempo, han
encarado [sus dilemas] y las consecuencias de los [mismos]”28. Como cierre a este párrafo
sólo nos queda aventurar nuestra propia definición, relacional como lo es el propio espaciotiempo29, donde: la historia es la humanidad transcurrida por el tiempo y la Historia el debate
intelectivo a lo que vamos conociendo y desconociendo de ese transcurso.
Como suele ocurrir, estas delimitaciones nos conducen a nuevas ilimitaciones. Nos asaltan así las preguntas del quién, qué, cómo, porqué, cuándo y dónde; seis uves dobles inglesas
periodísticas (el lead o quid de la cuestión noticiada) que ejercen de necesaria anamnesis
previa a toda investigación. Para el “quién” es fácil contestar que: el historiador; aunque ya
sabemos cuánto poder, fáctico y descarado, avasalla con sus discursos mediáticos la información diaria, ya que “en toda sociedad la producción del discurso está a la vez controlada,
seleccionada y redistribuida por cierto número de procedimientos que tienen por función
conjurar sus poderes y peligros, dominar el acontecimiento aleatorio y esquivar su pesada y
19 Zubiri, Xavier: Los problemas fundamentales de la metafísica occidental, Madrid, Alianza, 2003, p. 18. 20 Casado, Blas (coord.): Tendencias historiográficas actuales, Madrid, U.N.E.D., 2001, p. 47. Herodoto es
considerado el primer historiador porque sus historiaciones “se alejan de la época teocrática […al] narra[r] acontecimientos realizados por los hombres en un tiempo y un espacio determinado[s]”, ibídem. 21 Cicerón, Marco T.: De Oratore, México, U.N.A.M., 1995, libro II, cap. IX, ¶ 36. La expresión historia
magistra vitae acuñada por Cicerón se apoya en ejemplos helenísticos, como el de Polibio en sus: Historiai, Madrid,
Gredos, 1997, libro XII, cap. 25b y passim. 22 Gibbon, Edward: The History of the Decline and Fall of the Roman Empire, Londres, Methuen, 1909-1914
(vol. I), p. 69; muy en la línea de una declaración semejante de Voltaire, en su L’ingénue (1757). 23 Bloch, Marc: Introducción a la historia, México, F.C.E., 1992, p. 26. 24 Ortega y Gasset, José: Historia como sistema, Madrid, Revista de Occidente, 1975b, p. 97. 25 Sánchez-Albornoz, Claudio: Op. cit., p. 32. 26 Koselleck, Reinhart: Futuro pasado. Para una semántica de los tiempos históricos, Barcelona, Paidós,
1993, p. 333. 27 Mendieta, Eduardo: “La geografía de la utopía: regímenes espacio-temporales de la modernidad”, Cuadernos
Americanos, México, n.º 67 (enero-febrero de 1998), p. 238. 28 Klauer, Alfonso: ¿Leyes de la historia?, Lima, Eumed, 2005, p. 17. 29 O tiempoespacio, siguiendo la propuesta de Wallerstein, Immanuel: Impensar las ciencias sociales, México,
C.E.I.I.C.H.-U.N.A.M.-Siglo XXI, 1998 y passim. 
Norba. Revista de Historia, Vol. 20, 2007, 209-224
Historia e historia Pascual Raga Rosaleny 213
temible materialidad”30. Reciente ejemplo patrio de lo que acabamos de decir es el empeño de
aprobar una Ley de la Memoria Histórica, o el partidismo malicioso que se ha introducido en
varios libros de texto escolar en algunas comunidades autónomas, desvergonzadamente tendencioso. El qué presenta un amplísimo espectro, no solamente por lo mucho historiable que resta
por abordar sino también en el mejoramiento o enriquecimiento de lo ya historiado. El cómo
no carece tampoco de variedad, debiendo elegir según qué caso uno o varios de los múltiples
procedimientos a nuestro alcance, además del necesario auxilio de otras disciplinas incluyendo
la superación de la barrera establecida entre las llamadas “dos culturas”31; divorcio entre las
ciencias humanísticas y las naturales (o tecnológicas) que pretende remediarse actualmente
tanto de un lado como del otro32, imponiéndosenos pues un método ecléctico, acorde con lo
multiforme de nuestra empresa historiadora. El porqué es parte de la esencia del historiar,
tal decíamos supra, máxime cuando sabemos “que el mundo es complejo, y es cada vez más
complejo. […Y así,] la tarea de la ciencia no es reducir esta complejidad a una simplicidad
imposible, sino interpretar o explicar esta complejidad”33. El cuándo, ligado al dónde como ya
veíamos a propósito del tiempoespacio –vid. nota 29–, son asimismo labores fundamentales
para la Historia, por lo cual las comentamos a continuación, ya que su ubicación y relación
están en la base de nuestra ciencia.
La Historia es también una cuestión de perspectiva34. Conviene para su eficacia la visión
de conjunto tanto como la del menudeo al detalle, practicando al tiempo la exhaustividad,
Çsin descuidar ninguno de estos parámetros. Imaginemos por un momento la historia, o la
parte de ella que pretendamos abordar, bajo la apariencia de un grandísimo tapiz pictórico.
Pegados a pocos centímetros de la tela y recorriendo su extensión captaremos muchos de
sus detalles menudos, mas la visión de conjunto, por su misma enormidad se nos escapará,
debiendo alejarnos hasta una distancia adecuada para poder captar el sentido de su gran
dibujo35. Combinar ambas visiones será por tanto el único medio de poder entender lo allí
plasmado. Y todavía nos faltará un escrutinio más, el de observar el reverso de nuestro
metafórico tapiz, poblado de anudaciones y entrelazamientos constructores de la urdimbre
causacional y acontecimiental que es la historia. Este tipo de análisis, este concienzudo
trabajo multidisciplinar, es el propio de quien se dedica a la Historia de forma científica y
compromiso deontológico.
Aunque la Historia no se categoriza como disciplina autónoma profesionalizada hasta el
siglo xix, ya hemos visto cómo teníamos a Herodoto tal primer historiador, además de las
aportaciones anteriores de los logógrafos, así como las referencias históricas de la Biblia y sin
que sean despreciables las mitologías, pues, siguiendo a Elías: “términos como racionalidad
e irracionalidad no son […] opuestos polares […]. La vía para descubrir lo que es congruente
con la realidad puede conducir a través de toda una serie de supuestos imaginativos que
30 Foucault, Michel: El orden del discurso, Barcelona, Tusquets, 2002, p. 14. 31 En 1959 Charles P. Snow dictó una conferencia que a continuación publicaría (Two Cultures), donde denunciaba la segmentación de la investigación en dos ramas sin conexión. Vid. Snow, Charles P.: Las dos culturas y un
segundo enfoque, Madrid, Alianza, 1977. 32 Wallerstein, Immanuel: El fin de las certidumbres en ciencias sociales, México, C.E.I.I.C.H.-U.N.A.M.-
Siglo XXI, 1999, y Prigogine, Ilya: La nueva alianza, Madrid, Alianza, 1990, entre otros. 33 Walllerstein, Immanuel, citado en Valencia, Guadalupe: Entre cronos y kairós. Las formas del tiempo
sociohistórico, Barcelona, Anthropos, 2007, p. 120. 34 Ortega y Gasset, José: El tema de nuestro tiempo, Madrid, Espasa-Calpe, 1980, pp. 82-90. 35 Cfr. Gumilev, Lev N.: La búsqueda de un reino imaginario, Barcelona, Crítica, 1994, pp. 13-20, donde el
autor nos propone la idea (p. 16) de un “ ‘historioscopio’, [tal una especie de] instrumento que dispusiera de una
escala con grados de aproximación” histórica (el entrecomillado es nuestro), con el fin de ir realizando las aproximaciones pertinentes a los aconteceres que queremos estudiar.
214 Pascual Raga Rosaleny Historia e historia
Norba. Revista de Historia, Vol. 20, 2007, 209-224
tengan el carácter dominante de fantasías”36. Asimismo, san Agustín (354-430) ya afirmaba
que el trasunto de la Historia eran las actuaciones humanas, mas la historia, la ipsa historia
(la historia en sí misma), no es una creación humana puesto que procede de Dios37. Lo que
el santo de Hipona refiere como una formulación teológica es traspolación de lo que venimos
exponiendo: la Historia como investigación y representación de lo ocurrido en la historia. Por
otra parte, la juventud de la Historia puede ser responsable de no haberse “atrevido a entablar
una discusión sobre su proceso de autoconstitución […, que pasaría por] someterlo a una crítica
en sentido kantiano”38; mas ésta es tarea para ir realizando sin que per se invalide ni legitime
nada su inconclusión. También la química fue antes alquimia, la filosofía parte de la teología
o la astronomía derivó de la astrología.
3.  Teoría y praxis
Antes de abordar la tesis propiamente dicha conviene aclarar ciertas particularidades,
como son las referidas al intríngulis de las ciencias humanas y sus carencias. La Historia, al
igual que la sociología, la psicología o la antropología por citar algunas, estudia a los seres
humanos y sus relaciones, ámbito éste mudable por naturaleza además de que la persona es
el único ser que posee, además de otras características: “…o ánimo, aliento, deseo, gusto,
sentimiento, pasión, afecto y un largo etcétera de términos con los cuales se intenta verter en
castellano el término griego”39; dicho altísimo factor de impredecibilidad dificulta no poco todo
estudio emprendido de la metamórfica realidad humana, mas el factor “libertad” no anula los
patrones constatables, aunque diversifique enormemente los resultados al tiempo que implica
en cualquier proceso a examen un elevadísimo índice de variables a tomar en cuenta, ya
que “tras el orden aparente de los valores [hemos de buscar] el orden oculto de los vectores,
pues éste [nos] enseñará más sobre el primero que a la inversa”40. Otra complicación es la
derivada del principio de indeterminación postulado por Werner Heisenberg (1927), que para
el caso del historiador deviene en que éste: “lo mismo que los demás individuos, es también
un fenómeno social, producto a la vez que portavoz consciente o inconsciente de la sociedad
a la que pertenece; en concepto de tal, se enfrenta con los hechos del pasado histórico”41;
gravamen el susodicho que afecta por igual a toda investigación científica y que en el caso
de las ciencias humanas llega a concretarse en la dificultad añadida de confundir sujeto y
objeto42. Contra este óbice último, invocador de la subjetividad como invalidadora del trabajo
del historiador, traemos a colación una reflexión ricoeuriana ad hoc, “porque una cosa es la
motivación subjetiva de un oficio y otra cosa la estructura metódica que asegura su autonomía.
Eso es precisamente la objetividad: una obra de la actividad metódica”43.
36 Elias, Norbert: Op. cit., p. 127. 37 Agustín de Hipona: “De Doctrina Cristiana”, Opera omnia (vol. 34), Roma, Città Nuova, 2003, p. 44. 38 Oncina, Faustino, en la “Introducción” a Koselleck, Reinhart: Aceleración, prognosis y secularización,
Valencia, Pre-Textos, 2003, p. 33. 39 Gómez, Víctor: Entre lobos y autómatas. La causa del hombre, Madrid, Espasa Calpe, 2006, p. 49. 40 Debray, Régis: El Estado seductor. Las revoluciones mediológicas del poder, Buenos Aires, Manantial,
1995, p. 12. 41 Carr, Edward H.: ¿Qué es la historia?, Barcelona, Ariel, 1987, p. 93. 42 No en vano “la relación sujeto-objeto no es […] epistemológica, sino histórico-ontológica. [Ya que] el sujeto
es la idiosincrasia de la interpretación de la experiencia humana en el mundo [así como su] autoexperiencia”:
Heller, Ágnes: Historia y futuro, Barcelona, Península, 2000, p. 200. 43 Ricoeur, Paul: Historia y verdad, Madrid, Encuentro, 1990, pp. 13 y 25.
Norba. Revista de Historia, Vol. 20, 2007, 209-224
Historia e historia Pascual Raga Rosaleny 215
Las carencias aludidas supra tienen que ver con el método investigador, ya que, como
denunciara Snow en su ensayo citado, hay una serie de diferencias entre los paradigmas escrutadores de ambas “culturas” susceptibles de mejorarlas científicamente vía su interacción. Así,
la investigación humanística se interesa por objetos individuales y grupales y las relaciones
entre ellos, mientras que en la contrapartida naturalista el interés recae en las propiedades y
atributos de los objetos y por las leyes que éstos siguen. Este primer punto ya es muy interesante para su explotación interactiva, además de que son muchas las incursiones hechas en
este sentido desde, por ejemplo, la Historia total, la Historia estructural, la Nueva Historia, la
Historia conceptual o la Histórica, punto aperturador de un campo abonado para que se vaya
tanto a la epifanía como a la esencia. Otra diferencia reside en lo atañente al estudio de los
objetos, que es intenso pero sobre un número pequeño en la humanística y particularmente
extensivo en la natural; en el primero de los casos la escasez de lo investigado suscita dudas
de su validez fuera del grupo inquirido44 aunque, para el segundo tipo pesquisador, la superior cantidad en la “muestra” elegida tampoco certifica su perfecta extrapolación a la población entera (o en cualquier parte) allí representada45. En la indagación humanística interesa el
desarrollo de los objetos entretanto que en las ciencias naturales los cambios objetuales son
menores. Para las ciencias sociales el estudio de sus objetos es “de campo”, siendo generalmente artificial en la contrapartida de las ciencias naturales. En las explicativas humanísticas
se procede por futurición y en las naturalistas por preterición46. Finalmente, en las humanidades
se estudian principalmente cualidades y en las naturales cantidades.