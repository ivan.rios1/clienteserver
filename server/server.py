import zmq
import json
import uuid
import os

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind('tcp://*:8888')

CHUNK_SIZE = 1000

def leeDATABASE():
    file = open("./DATABASE.json", "r")
    data=file.read()
    DATABASE = json.loads(data)
    file.close()
    return DATABASE

def actualizaDB():
    file = open("./DATABASE.json", "w")
    appendjson = json.dumps(DATABASE, indent=4)
    file.write(appendjson)
    file.close()

def nuevoDict(json_dic, link):
    filename = json_dic["filename"]
    newdic = {link : filename}
    return newdic

def nuevoUsuario(json_dic, link):
    nombre = json_dic["usuario"]
    filename = json_dic["filename"]
    newuser =   { nombre : {link : filename}}
    return newuser

def procesaJson(mensjson):
    jsondecoded = mensjson.decode('utf-8')
    finaljson = json.loads(jsondecoded)
    return finaljson

def checkFilename(json_dict, DATABASE):
    existe = False
    nombreasubir = json_dict["usuario"]
    archivoasubir = json_dict["filename"]
    for nombres, archivos in DATABASE.items():
        for link, filename in archivos.items():
            if nombreasubir == nombres and archivoasubir == filename:
                existe = True
    return existe

def upload(json_dic):
    nombre = json_dic["usuario"]
    file_dir = json_dic["filename"]
    link = str(uuid.uuid4())
    if nombre in DATABASE:
        newjson = nuevoDict(json_dic, link)
        DATABASE[nombre].update(newjson)
        actualizaDB()
        response = f'\nSe agregó el archivo:{file_dir} al usuario {nombre}\n'
        socket.send_string(response)
        print('[SERV] archivo agregado')
    else:
        newuser = nuevoUsuario(json_dic, link)
        DATABASE.update(newuser)
        actualizaDB()
        response = f'\nnuevo usuario {nombre} con archivo {file_dir}\n'
        socket.send_string(response)
        print('[SERV] usuario y carpeta creados y archivo agregado')

def sizeArchivo(usuario,nombrearchivo):
    newfilepath = './Serverfiles/'+usuario+'/'+nombrearchivo
    file = open(newfilepath, "rb")
    file.seek(0, os.SEEK_END)
    size = file.tell()
    file.seek(0, os.SEEK_SET)
    return size

def segmentaArchivo(usuario, nombrearchivo, chunk):
    newfilepath = './Serverfiles/'+usuario+'/'+nombrearchivo
    file = open(newfilepath, "rb")
    file.seek(chunk)
    data= file.read(CHUNK_SIZE)
    file.seek(0, os.SEEK_SET)
    file.close()
    return data

def download(DATABASE,dllink,chunk):
    encontrado = False
    nombrearchivo =''
    usuario = ''
    for nombres, items in DATABASE.items():
        for link, filename in items.items():
            if link == dllink:
                nombrearchivo = filename
                usuario = nombres
                response = f'\nHa solicitado descargar {filename} de {nombres}\n'
                encontrado = True

    if encontrado:
        size = sizeArchivo(usuario, nombrearchivo)
        json_response = json.dumps(
            {
                'encontrado': True,
                'response': response,
                'filename': nombrearchivo,
                'size': size
            }
        )
        json_response_encoded = json_response.encode('utf-8')
        filesegment = segmentaArchivo(usuario,nombrearchivo, chunk)
        socket.send_multipart([json_response_encoded,filesegment])
    else:
        response = '\nlink no encontrado'
        json_response = json.dumps(
            {
                'encontrado': False,
                'response': response
            }
        )
        json_response_encoded = json_response.encode('utf-8')
        socket.send_multipart([json_response_encoded])
        print('[SERV] link no encontrado descarga no exitosa')

def shareLink(json_dic, DATABASE):
    linkshare=''
    nombreacompartir = json_dic["usuario"]
    archivoacompartir = json_dic["filename"]
    for nombres, archivos in DATABASE.items():
        for link, filename in archivos.items():
            if nombreacompartir == nombres and archivoacompartir == filename:
                linkshare = link
    return linkshare

def listadorArchivos(DATABASE):
    lista = ''
    for nombres, archivos in DATABASE.items():
        lista += nombres+':\n'
        for link, filename in archivos.items():
            lista += '      -'+filename + '\n'
    socket.send_string(lista)
    print('[SERV] enviada lista de archivos')

def listadorArchivosUsuario(json_dic,DATABASE):
    usuario = json_dic["usuario"]
    lista = f'archivos de {usuario}: \n'
    encontrado = False
    for nombres, archivos in DATABASE.items():
        if usuario == nombres:
            lista += nombres+':\n'
            encontrado = True
        for link, filename in archivos.items():
            if usuario == nombres:
                lista += '      -'+filename + '\n'
    if encontrado:
        print('[SERV] enviando archivos al cliente ')
        socket.send_string(lista)
    else:
        print('[SERV] Usuario no encontrado')
        socket.send_string('Usuario no encontrado')

def cargaChunks(json_dic, archivo):
    newfilepath = './Serverfiles/'+json_dic["usuario"]+'/'+json_dic["filename"]
    os.makedirs(os.path.dirname(newfilepath), exist_ok=True)
    file = open(newfilepath, "ab")
    file.write(archivo)
    file.close()


while True:
    mens = socket.recv_multipart()
    DATABASE= leeDATABASE()
    json_dic = procesaJson(mens[0])
    if json_dic["tipo"] == 'upload':
        chunk = mens[1]
        existe = checkFilename(json_dic, DATABASE)
        if existe:
            cargaChunks(json_dic, chunk)
            socket.send_string('True')
        else:
            upload(json_dic)
            cargaChunks(json_dic, chunk)
    if json_dic["tipo"] == 'sharelink':
        if checkFilename(json_dic, DATABASE):
            linkshare = shareLink(json_dic, DATABASE)
            socket.send_string(f'\nlink para descargar {json_dic["filename"]} es: \n    {linkshare}\n')
            print('[SERV] link compartido')
        else:
            socket.send_string('archivo no encontrado')
            print('[SERV] archivo no encontrado')
    if json_dic["tipo"] == 'list':
        if json_dic["filename"] == 'todo':
            listadorArchivos(DATABASE)
        else:
            listadorArchivosUsuario(json_dic, DATABASE)
    if json_dic["tipo"] == 'downloadlink':
        dllink = json_dic["filename"]
        chunkjson = json.loads(mens[1])
        chunk = chunkjson["chunk"]
        download(DATABASE, dllink, chunk)