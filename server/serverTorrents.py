import os
import socket
import uuid
import random
import string
import hashlib
import zmq


context = zmq.Context()
client_socket = context.socket(zmq.REP)
client_socket.bind('tcp://*:1111')

MAC=str(hex(uuid.getnode()))
IP=socket.gethostbyname(socket.gethostname())
PID=str(os.getpid())
RAND=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(100))
S=IP+MAC+PID+RAND
sha1 = hashlib.sha1()
sha1.update(S.encode('utf-8'))
Shashed= sha1.hexdigest()
dec=int(Shashed, 16)
print(f'String = IP + MAC + PID + RAND \n')
print(f'String = {S} \n')
print(f'Hash = {Shashed} \n')
print(f'decimal = {dec} \n')


def inicializar():
    port = '8001'
    server = '1'
    range = '[0,9]'
    return port, server, range

def menuDatos():
    os.system('cls||clear')
    print('1.Inicializar\n2.Conectar\n')
    print('Seleccione una opcion: ')
    selector = str(input())
    os.system('cls||clear')
    if selector == '1':
        os.system('cls||clear')
        port,server,range = inicializar()
        print(f'\nPuerto asignado {port}\n')
        print(f'Servidor No: {server}\n')
        print(f'Rango asignado {range}\n')
    elif selector == '2':
        pass
    else:
        print('digite correctamente el comando')

while True:
    menuDatos()
    str(input('\n\npresione enter para continuar'))